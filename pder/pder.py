# TODO: return to freeplay key

import pyaudio
import wave
import sys
from time import *
import termios, fcntl, os
from threading import Timer
from subprocess import Popen, PIPE
from StringIO import StringIO
import io
import liblo # for OSC
import random

DEBUG = False

notes_paths = {}
cached_notes = {}
tunes = {}
SOUND_START  = "sounds/start.wav"
SOUND_FAIL   = "sounds/fail.wav"
SOUND_LISTEN = "sounds/listen1.wav"
SOUND_FIRE   = "sounds/fire1.wav"
SOUND_WON    = "sounds/dubstep.wav"
devnull = open('/dev/null', 'w')
z_vector_ip = "192.168.1.61"
z_vector_port = 9000

def play_sound_external(path):
    #p = Popen(['aplay', path])
    # TODO: supress stdout
    p = Popen(['aplay'], stdin=PIPE, stdout=devnull, stderr=devnull)
    #p.communicate(cached_sounds[path].getvalue())
    p.communicate(cached_notes[path])

def play_sound_async_external(path, delay=0):
    t = Timer(delay, play_sound_external, [path])
    t.start()

def kbhit():
    fd = sys.stdin.fileno()
    oldterm = termios.tcgetattr(fd)
    newattr = termios.tcgetattr(fd)
    newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
    termios.tcsetattr(fd, termios.TCSANOW, newattr)
    oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)
    try:
        while True:
            try:
                c = sys.stdin.read(1)
                return c
            except IOError:
                return None
    finally:
        termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)


def ignore_keyboard():
    while kbhit() is not None:
        pass


def make_message_auto(path, *args):
    msg = liblo.Message(path)

    for a in args:
        try: v = int(a)
        except ValueError:
            try: v = float(a)
            except ValueError:
                v = a
        msg.add(v)

    return msg


def make_message_manual(path, types, *args):
    if len(types) != len(args):
        sys.exit("length of type string doesn't match number of arguments")

    msg = liblo.Message(path)
    try:
        for a, t in zip(args, types):
            msg.add((t, a))
    except Exception, e:
        sys.exit(str(e))

    return msg


def test_z_vector():
    try:
        z_vector = liblo.Address(z_vector_ip, z_vector_port)
    except liblo.AddressError, err:
        print(str(err))
        return

    # msg = make_message_manual(sys.argv[2], sys.argv[3][1:], *sys.argv[4:])

    args = [20]
    msg = make_message_auto("/Z Vector/parameter_name", *args)

    try:
        liblo.send(z_vector, msg)
    except IOError, e:
        print(str(e))

test_z_vector()

class PderGame:
    STATUS_FREEPLAY, STATUS_START, STATUS_LOSE, STATUS_WON = xrange(0, 4)
    INPUT_TYPE_KEYBOARD, INPUT_TYPE_OSC = xrange(0, 2)

    status      = STATUS_FREEPLAY
    speed       = 1
    indication  = False
    input_type  = None
    osc_note    = None
    osc_end     = False

    def run(self):
        while True:
            if self.status is self.STATUS_FREEPLAY:
                # dispatch OSC messages
                self.server.recv(1)
                # handle keyboard input
                self.handle_keyboard()

            elif self.status is self.STATUS_START:
                shuffled = tunes.keys()
                random.shuffle(shuffled)
                for name, tune in ((name, tunes[name]) for name in shuffled):
                    print("Tune is {0}".format(name))
                    completed = self.process_tune(tune)
                    if not completed:
                        self.status = self.STATUS_LOSE
                        break
                    else:
                        self.play_completed_sound()
                if self.status is not self.STATUS_LOSE:
                    self.status = self.STATUS_WON
                    ignore_keyboard()

            elif self.status is self.STATUS_LOSE:
                self.play_lose_sound()
                self.status = self.STATUS_FREEPLAY
                ignore_keyboard()

            elif self.status is self.STATUS_WON:
                self.play_won_sound()
                self.status = self.STATUS_FREEPLAY
                ignore_keyboard()

    def get_note(self, tune, position):
        # (sequence, speed) -> (note, delay) -> (note)
        return tune[0][position][0]

    def get_played_note(self, input_type):
        if input_type is self.INPUT_TYPE_KEYBOARD:
            c = kbhit()
            if c == ' ':
                #print("AAAA")
                return None, True
            elif c is not None:
                return self.map_keyboard_to_note(c), False
        elif input_type is self.INPUT_TYPE_OSC:
            self.server.recv(1)
            #print("returning {0}, {1}".format(self.osc_note, self.osc_end))
            return self.osc_note, self.osc_end
        return None, False


    def process_tune(self, tune):
        self.play_start_sound()
        self.play_tune(tune, has_indication=True)
        self.progress = 1
        sleep(1.2)

        while True:
            if self.progress > len(tune[0]):
                return True

            self.play_listen_sound()
            sleep(0.5)
            self.play_tune(tune, self.progress, self.indication)
            sleep(0.5)
            self.play_fire_sound()

            # ignore all keyboard events up to this moment
            if self.input_type is self.INPUT_TYPE_KEYBOARD:
                ignore_keyboard()

            num_played_notes = 0
            while True:
                note, is_end = self.get_played_note(self.input_type)

                if is_end is True:
                    #print("BBB")
                    self.status = self.STATUS_FREEPLAY
                    self.osc_note = None
                    self.osc_end = None
                    self.osc_input_url = None
                    return

                elif note is not None:

                    self.osc_note = None

                    note_correct = self.get_note(tune, num_played_notes)
                    #print(type(note), type(note_correct))
                    if note == note_correct:
                        # TODO: indication
                        print("correct note {0}".format(note))
                        # this note is correct, wait for next
                        num_played_notes += 1
                        # this tune is completed
                        if num_played_notes == self.progress:
                            play_sound_external(notes_paths[note])
                            # we are making some progress
                            self.progress += 1
                            break
                        play_sound_async_external(notes_paths[note])

                    else:
                        # TODO: indication
                        print("wrong note {0} should be {1}".format(note, note_correct))
                        play_sound_async_external(notes_paths[note])
                        sleep(0.1)
                        self.play_fail_sound()
                        break
        return False


    def play_tune(self, tune, progress=-1, has_indication=False):
        sequence, speed = tune
        # play whole tune if negative progress position
        if progress <= 0:
            progress = None
        # compute delay factor in seconds
        delay_factor = 0.5 / speed
        # iterate over notes
        for note, delay in sequence[:progress][:-1]:
            play_sound_async_external(notes_paths[note])
            if has_indication:
                self.indicate_note(note)
            sleep(delay * delay_factor)
        # last note in tune subsequence don't have delay after it
        play_sound_async_external(notes_paths[sequence[:progress][-1:][0][0]])
        if has_indication:
            self.indicate_note(note)

    def indicate_note(self, note):
        # TODO
        pass

    def play_start_sound(self):
        play_sound_external(SOUND_START)
    def play_listen_sound(self):
        play_sound_external(SOUND_LISTEN)
        pass
    def play_fire_sound(self):
        play_sound_external(SOUND_FIRE)
    def play_fail_sound(self):
        play_sound_external(SOUND_FAIL)
    def play_completed_sound(self):
        play_sound_external(SOUND_WON)
        pass
    def play_lose_sound(self):
        pass
    def play_won_sound(self):
        play_sound_external(SOUND_WON)

    def blob_to_hex(self, b):
        return " ".join([ (hex(v/16).upper()[-1] + hex(v%16).upper()[-1]) for v in b ])

    # osc_to_note_map =  {"/leftHighCut":    "A",
    #                     "/leftMidCut":     "B",
    #                     "/leftLowCut":     "C",
    #                     "/rightHighCut":   "D",
    #                     "/rightMidCut":    "E",
    #                     "/rightLowCut":    "F"}

    osc_to_note_map =  {"/C":       "C",
                        "/D":       "D",
                        "/E":       "E",
                        "/F":       "F",
                        "/G":       "G",
                        "/A":       "A",
                        "/B":       "B",
                        "/Csharp":  "Db",
                        "/Dsharp":  "Eb",
                        "/Fsharp":  "Gb",
                        "/Gsharp":  "Ab",
                        "/Asharp":  "Bb",

                        "/C1":      "C1",
                        "/D1":      "D1",
                        "/E1":      "E1",
                        "/F1":      "F1",
                        "/G1":      "G1",
                        "/A1":      "A1",
                        "/B1":      "B1",
                        "/Csharp1": "Db1",
                        "/Dsharp1": "Eb1",
                        "/Fsharp1": "Gb1",
                        "/Gsharp1": "Ab1",
                        "/Asharp1": "Bb1",
                        }

    keyboard_to_note_map = {"q":           "C",
                            'w':           "D",
                            'e':           "E",
                            'r':           "F",
                            't':           "G",
                            'y':           "A",
                            'u':           "B",
                            '2':           "Db",
                            '3':           "Eb",
                            '5':           "Gb",
                            '6':           "Ab",
                            '7':           "Bb",

                            "z":           "C1",
                            'x':           "D1",
                            'c':           "E1",
                            'v':           "F1",
                            'b':           "G1",
                            'n':           "A1",
                            'm':           "B1",
                            's':           "Db1",
                            'd':           "Eb1",
                            'g':           "Gb1",
                            'h':           "Ab1",
                            'j':           "Bb1"}

    def map_osc_message_to_note(self, msg):
        try:
            return self.osc_to_note_map[msg]
        except KeyError:
            return None

    def map_keyboard_to_note(self, c):
        try:
            return self.keyboard_to_note_map[c]
        except KeyError:
            return None

    def osc_server_callback(self, path, args, types, src):
        write = sys.stdout.write
        ## print source
        #write("from " + src.get_url() + ": ")
        # print path
        #write(path + " ,")
        #write(path)

        if self.status is self.STATUS_FREEPLAY:
            note = self.map_osc_message_to_note(path)
            if note is not None:
                play_sound_async_external(notes_paths[note])
            elif path == "/Game":
                print("Starting new game with client {0} using OSC input".format(src.get_url()))
                self.status = self.STATUS_START
                self.input_type = self.INPUT_TYPE_OSC
                self.osc_input_url = src.get_url()
        elif self.status is self.STATUS_START:
            print("osc callback has been called while playing game")
            if src.get_url() != self.osc_input_url:
                return
            self.osc_note = self.map_osc_message_to_note(path)
            if path == "/Game":
                print("Ending new game with client {0} using OSC input".format(src.get_url()))
                self.osc_end = True


        # write(types)
        # for a, t in zip(args, types):
        #     write(" ")
        #     if t == None:
        #         #unknown type
        #         write("[unknown type]")
        #     elif t == 'b':
        #         # it's a blob
        #         write("[" + self.blob_to_hex(a) + "]")
        #     else:
        #        # anything else
        #         write(str(a))
        # write('\n')

    def handle_keyboard(self):
        c = kbhit()
        if c is not None:
            if DEBUG:
                print("Hit me baby one more time: {0}".format(c))

            note = self.map_keyboard_to_note(c)
            if note is not None:
                play_sound_async_external(notes_paths[note])
            elif c == ' ':
                print("Starting new game using keyboard input")
                self.status = self.STATUS_START
                self.input_type = self.INPUT_TYPE_KEYBOARD

    def __init__(self, port = None):
        # create server object
        try:
            self.server = liblo.Server(port)
        except liblo.ServerError, err:
            sys.exit(str(err))

        print("listening on URL: " + self.server.get_url())

        # register callback function for all messages
        self.server.add_method(None, None, self.osc_server_callback)

        # self.play_tune(tunes["Smoke on the Water.txt"])
        # sleep(1)
        # self.play_tune(tunes["Jingle Bells.txt"])
        # sleep(1)
        # self.play_tune(tunes["Tetris.txt"])
        # sleep(1)
        # self.play_tune(tunes["WWW.txt"])
        # sleep(1)
        # self.play_tune(tunes["Boomer.txt"])

        #self.play_tune(tunes["Steel Panther.txt"])
        #self.play_tune(tunes["reed.txt"])


def GPIO():
    pass


def read_tune(path):
    f = open(path, 'r')
    # read first line without getting '\r'/'\n' in the end
    s = f.read().splitlines()[0]

    # tokens
    tokens = s.split(" ")
    tokens.append("0")
    if DEBUG:
        print tokens

    # tune sequence
    tune = []
    for i in xrange(0, len(tokens), 2):
        if DEBUG:
            print tokens[i], tokens[i + 1]
        tune.append(tuple((tokens[i], float(tokens[i + 1]))))

    if DEBUG:
        print tune

    return tune


def init_tunes():
    tunes_files = [#("Smoke on the Water.txt", 1.1),
                   #("Jingle Bells.txt", 1.5),
                   #("Boomer.txt", 2.4),
                   #("Tetris.txt", 6),
                   ("Steel Panther.txt", 2),
                   ("reed.txt", 2.2),
                   ("WWW.txt", 1.8)]
    for file, speed in tunes_files:
        tunes[file] = (read_tune("".join(["tunes/", file])), speed)
    if DEBUG:
        print tunes


def init_notes():
    global notes_paths
    global cached_notes

    # files = ["Abmaj7_F.wav", "Amaj7_F.wav", "Bbmaj7_F.wav", "Bmaj7_F.wav", "Cmaj7_F.wav", "Dbmaj7_F.wav",
    #          "Dmaj7_F.wav", "Ebmaj7_F.wav", "Emaj7_F.wav", "Fmaj7_F.wav", "Gbmaj7_F.wav", "Gmaj7_F.wav"]
    files = ["Ab.wav", "A.wav", "Bb.wav", "B.wav", "C.wav", "Db.wav",
             "D.wav", "Eb.wav", "E.wav", "F.wav", "Gb.wav", "G.wav",
             "Ab1.wav", "A1.wav", "Bb1.wav", "B1.wav", "C1.wav", "Db1.wav",
             "D1.wav", "Eb1.wav", "E1.wav", "F1.wav", "Gb1.wav", "G1.wav"]

    f = []
    for i in files:
        f.append("".join(["notes/", i]))

    notes_paths = {"Ab": f[0], "A": f[1], "Bb": f[2], "B": f[3], "C": f[4], "Db": f[5],
                   "D": f[6], "Eb": f[7], "E": f[8], "F": f[9], "Gb": f[10], "G": f[11],
                   "Ab1": f[12], "A1": f[13], "Bb1": f[14], "B1": f[15], "C1": f[16], "Db1": f[17],
                   "D1": f[18], "Eb1": f[19], "E1": f[20], "F1": f[21], "Gb1": f[22], "G1": f[23]}

    for note, path in notes_paths.iteritems():
        #file = open(path, 'rb')
        #cached_sounds[path] = StringIO(file.read()).getvalue()
        #file.close()

        fbuf = io.open(path, 'rb')
        cached_notes[path] = fbuf.read()

        #a = array.array('c')
        #cached_sounds[path] = a.fromfile(path, n)

    #play_sound_async_external(sounds["A"])
    # while(1):
    #     for note, file in sounds.iteritems():
    #         play_sound_async_external(file)
    #         sleep(0.5)


def load_sound(path):
    global cached_notes
    fbuf = io.open(path, 'rb')
    cached_notes[path] = fbuf.read()


def init_sounds():
    load_sound(SOUND_START)
    load_sound(SOUND_FAIL)
    load_sound(SOUND_LISTEN)
    load_sound(SOUND_FIRE)
    load_sound(SOUND_WON)
    # play_sound_external(SOUND_FAIL)
    # play_sound_external(SOUND_LISTEN)
    # play_sound_external(SOUND_FIRE)


def init():
    init_tunes()
    init_notes()
    init_sounds()


if __name__ == '__main__':

    init()

    #app = PderGame(12102)
    app = PderGame(9001)

    try:
        app.run()
    except KeyboardInterrupt:
        del app
